# DonneTonAvis
<p><a href="https://symfony.com" target="_blank">
    <img src="https://symfony.com/logos/symfony_black_02.svg">
</a></p>
The project DonneTonAvis is a blog's project developped on Symfony 4.4 .


## Features
* Create an user via the registration path
* login/logout
* Create, Read, update and delete an article
* Comment an article
* administration panel available


## Setup
* composer < 2 (recommand)
* Php <= 7.4



## Install Project

```
git clone https://gitlab.com/Roryyyy/donnetonavis.git

cd 

-- if you wish to contribuate and create a new branch to do so
git checkout -b name_of_the_new_branch

composer install

```

## Setup Database

Use your own database setup on a .env.local file

Create Database
------------

```
php bin/console doctrine:database:create
```


## Start the project
from your IDE's terminal:

```
symfony serve
```


You're all set :-)
-----------------------

for more information go on the official Symfony website:

* [Symfony](https://symfony.com/doc/4.4/index.html.html)


