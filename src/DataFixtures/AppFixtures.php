<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for($i=1; $i<11; $i++){
            $user = new User();
            $user->setPseudo($faker->userName)
                ->setRoles(['ROLE_USER'])
                ->setPassword($faker->password)
                ->setEmail($faker->email);
            $manager->persist($user);
        }

        $manager->flush();
    }
}
