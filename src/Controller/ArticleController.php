<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\CommentFormType;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class ArticleController extends AbstractController
{
    /**
     * @Route("/articles", name="articles")
     * @param ArticleRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index(ArticleRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {
        $articles = $paginator->paginate(
            $repository->findAllWithPagination(),
            $request->query->getInt('page', 1),
            6
        );
        return $this->render('article/article.html.twig', [
            "articles" => $articles
        ]);
    }

    /**
     * @Route("/articles/VIP/", name="get_articles_exclusives")
     * @Security("is_granted('ROLE_USER')",
     *     message="Vous devez être log consulter les articles exclusifs !")
     * @param ArticleRepository $repository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return mixed
     */
    public function getExclusivesArticles(ArticleRepository $repository, Request $request, PaginatorInterface $paginator)
    {
        $articles = $paginator->paginate(
            $repository->findBy([
                'vip' => 1
            ],
                [
                    'id' => 'DESC',
                ]),
            $request->query->getInt('page', 1),
            9
        );

        return $this->render('article/oneArticle.html.twig', [
            'articles' => $articles
        ]);

    }

    /**
     * Create post
     * @route("/article/new", name="create_post")
     * @Security("is_granted('ROLE_USER')",
     *     message="Vous devez vous log pour créer un article !")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return mixed
     */
    public function createPost(Request $request, EntityManagerInterface $em)
    {
        $article = new Article();

        $formArticle = $this->createForm(ArticleType::class, $article);
        $formArticle->handleRequest($request);

        if ($formArticle->isSubmitted() && $formArticle->isValid()) {
            if ($this->isGranted('ROLE_USER')) {
                $article
                    ->setCategories($formArticle->get('category')->getData())
                    ->setUser($this->getUser());

                $em->persist($article);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Votre article a été publié'
                );

                return $this->redirectToRoute('profil');
            }
        }

        return $this->render('article/create_article.html.twig', [
            'formArticle' => $formArticle->createView()
        ]);
    }

    /**
     * @Route("/articles/{slug}", name="get_articles")
     * @param Article $article
     * @param ArticleRepository $repository
     * @param CommentRepository $commentRepository
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param EntityManagerInterface $em
     * @return mixed
     */
    public function getArticle(
        Article $article,
        ArticleRepository $repository,
        CommentRepository $commentRepository,
        Request $request,
        PaginatorInterface $paginator,
        EntityManagerInterface $em
    )
    {
        $var = [
            'slug' => $article->getSlug()
        ];

        if ($article->getVip() != 0 && !$this->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('login');
        }

        $article = $repository->findOneBy($var);
        $articles = $repository->findBy([], ['id' => 'DESC'], 3);


        $comments = $paginator->paginate(
            $commentRepository->findBy(['article' => $article], ['id' => 'DESC']),
            $request->query->getInt('page', 1),
            3
        );

        $comment = new Comment();
        $formComment = $this->createForm(CommentFormType::class, $comment);
        $formComment->handleRequest($request);

        if ($formComment->isSubmitted() && $formComment->isValid()) {
            $comment
                ->setUser($this->getUser())
                ->setArticle($article);

            $em->persist($comment);
            $em->flush();

            $this->addFlash(
                'success',
                'Votre commentaire a eté posté !'
            );

            return $this->redirectToRoute('get_articles', ['slug' => $article->getSlug()]);
        }

        $var = [
            'article' => $article,
            'articles' => $articles,
            'comments' => $comments,
        ];

        if ($this->isGranted('ROLE_USER')) {
            $var['formComment'] = $formComment->createView();
        }

        return $this->render('article/oneArticle.html.twig', $var);
    }

    /**
     * @Route("/article/edit/{slug}", name="update_article")
     * @Security("is_granted('ROLE_USER') or user === article.getUser()",
     *     message="Vous n'avez pas les droits pour modifier cet article !")
     * @param Article $article
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return mixed
     */
    public function updatePost(Article $article, Request $request, EntityManagerInterface $em)
    {
        $formArticle = $this->createForm(ArticleType::class, $article);
        $formArticle->handleRequest($request);

        if ($formArticle->isSubmitted() && $formArticle->isValid()) {
            $em->persist($article);
            $em->flush();

            $this->addFlash(
                'success',
                'Votre article a bien été modifié.'
            );

            return $this->redirectToRoute('get_articles', ['slug' => $article->getSlug()]);
        }

        return $this->render('article/create_article.html.twig', [
            'formArticle' => $formArticle->createView()
        ]);
    }

    /**
     * Delete Post
     * @Security("is_granted('ROLE_USER') and user === article.getUser() or is_granted('ROLE_ADMIN')",
     *     message="Vous ne pouvez pas supprimer cet article !")
     * @Route("/article/delete/{slug}/{_csrf_token}", name="delete_article")
     * @param Article $article
     * @param EntityManagerInterface $em
     * @param Request $request
     * @param CsrfTokenManagerInterface $tokenManager
     * @return mixed
     */
    public function deletePost(
        Article $article,
        EntityManagerInterface $em,
        Request $request,
        CsrfTokenManagerInterface $tokenManager
    )
    {
        $token = new CsrfToken('delete_article', $request->attributes->get('_csrf_token'));

        if ($tokenManager->isTokenValid($token)) {
            $em->remove($article);
            $em->flush();

            $this->addFlash(
                'success',
                "L'article {$article->getTitle()} a bien été supprimé"
            );

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        $this->addFlash(
            'danger',
            'Token invalid !'
        );

        return $this->redirectToRoute('get_articles');
    }

    /**
     * @Route("/articles/categories/{category}", name="app_posts_by_category")
     * @param ArticleRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param string $category
     * @return mixed
     */
    public function getPostsByCategory(
        ArticleRepository $repository,
        PaginatorInterface $paginator,
        Request $request,
        string $category
    )
    {
        $articles = $paginator->paginate(
            $repository->findByCategory($category),
            $request->query->getInt('page', 1)
        );

        return $this->render('article/articles.html.twig', [
            'articles' => $articles
        ]);
    }

}
